# WP-Utils

## Installation
```
composer require couleurcitron/wp-utils
```

## Usage

### Post Types

Register custom post types.

1. Extend the `CouleurCitron\WPUtils\PostType` abstract class and provide the same post type configuration as for the `register_post_type()` function.
    
    ```    
    use CouleurCitron\WPUtils\PostType;
    
    class MyCustomPostType extends PostType {
    
        /**
         * Post type configuration
         *
         * @see https://developer.wordpress.org/reference/functions/register_post_type/
         *
         * @return array
         */
        protected static function config() {
            return [
                //
            ];
        }
    
    }
    ```

2. Register the post type in your `functions.php`. No need to do it in a hook.

    ```
    MyCustomPostType::register();
    ``` 

### Taxonomies

Register custom taxonomies.

1. Extend the `CouleurCitron\WPUtils\Taxonomy` abstract class and provide the same taxonomy configuration 
    as for the `register_taxonomy()` function.
    
    Also provide a single (string) or multiple (array of string) object types for this taxonomy.  
    It can be the post type name (available with `MyCustomPostType::name()` for post types registered with this library)
    or the class FQDN (`MyCustomPostType::class`).
    
    ```    
    use CouleurCitron\WPUtils\Taxonomy;
    
    class MyCustomTaxonomy extends Taxonomy {
   
        /**
         * Set object type(s) for the taxonomy
         *
         * @return string|array
         */
        protected static function objectType() {
            //
        }
    
        /**
         * Taxonomy configuration
         *
         * @see https://developer.wordpress.org/reference/functions/register_taxonomy/
         *
         * @return array
         */
        protected static function config() {
            return [
                //
            ];
        }
    
    }
    ```

2. Register the taxonomy in your `functions.php`. No need to do it in a hook.

    ```
    MyCustomTaxonomy::register();
    ``` 

### Columns

Add columns in the admin posts list. Useful for displaying meta values.

```
use CouleurCitron\WPUtils\Column;

Column::make( 'custom_field', 'Custom Field' )
      ->label( 'Custom Field' ) // Optional if the ::make() second argument is provided
      ->order( 1 ) // Position of the column from the left, first column is at position 0
      ->sortable() // Optional, the sort() method automatically turns on sortability
      ->sort( function ( WP_Query $query ) { // Customize the WP_Query when the column is sortable
          $query->set( 'meta_key', 'custom_field' );
          $query->set( 'orderby', 'meta_value' );
      } )
      ->render( function ( int $post_id ) { // Display the column value
          echo get_post_meta( $post_id, 'custom_field', true );
      } )
      ->register( MyCustomPostType::name() /* or 'post' */ ); // Register the column to show on the given post type
```

For post types registered with this library, you can declare columns in the class by overriding the `columns()` method.

```
use CouleurCitron\WPUtils\Column;
use CouleurCitron\WPUtils\PostType;

class MyCustomPostType extends PostType {

    /**
     * Post type configuration
     *
     * @see https://developer.wordpress.org/reference/functions/register_post_type/
     *
     * @return array
     */
    protected static function config() {
        return [
            //
        ];
    }

    /**
     * @return \CouleurCitron\WPUtils\Column[]
     */
    protected static function columns() {
        return [
            // No need to call register() on the column, it will be automatically registered for this post type
            Column::make( 'custom_field', 'Custom Field' )
                  ->label( 'Custom Field' )
                  ->order( 1 )
                  ->sortable()
                  ->sort( function ( WP_Query $query ) {
                      $query->set( 'meta_key', 'custom_field' );
                      $query->set( 'orderby', 'meta_value' );
                  } )
                  ->render( function ( int $post_id ) {
                      echo get_post_meta( $post_id, 'custom_field', true );
                  } ),
        ];
    }

}
```

## Methods and properties

Methods returning multiples items will return an `Illuminate\Support\Collection` ([docs](https://laravel.com/docs/collections)).

### `CouleurCitron\WPUtils\PostType`

- `PostType::$posts_per_page;`  
    If not null, set the number of posts to display on the post type archive page.
    Otherwise, it will default to the global WordPress setting.

- `PostType::$translatable = false;`  
    Enable translation for this post type. (Only compatible with Polylang).

- `PostType::register(): void;`  
    Register the post type. No need to call it in a hook.

- `PostType::name(): string;`  
    Get the post type name (e.g. `'my-custom-post-type'`).

- `PostType::all( array $args = [] ): Illuminate\Support\Collection;`  
    Performs a query for this post type. Return all posts by default.

- `PostType::insert( array $args ): WP_Post;`  
    Insert a new post for this post type. `$args` is the same as `wp_insert_post()`.

### `CouleurCitron\WPUtils\Taxonomy`

- `PostType::$posts_per_page;`  
    If not null, set the number of posts to display on the term page.
    Otherwise, it will default to the global WordPress setting.

- `PostType::$translatable = false;`  
    Enable translation for this taxonomy. (Only compatible with Polylang).

- `Taxonomy::register(): void;`  
    Register the taxonomy. No need to call it in a hook.

- `Taxonomy::name(): string;`  
    Get the post type name (e.g. `'my-custom-taxonomy'`).

- `Taxonomy::all( array $args = [] ): Illuminate\Support\Collection;`  
    Performs a tax query for this taxonomy. Return all terms by default.

- `Taxonomy::find( int $id ): WP_Term;`  
    Find a term of this taxonomy by id.

- `Taxonomy::fromPost( int $id ): Illuminate\Support\Collection;`  
    Find all terms of this taxonomy for a given post.

- `Taxonomy::insert( string $term, array $args = [] ): WP_Post;`  
    Insert a new term for this taxonomy. Arguments are the same as `wp_insert_term()`.
