<?php

namespace CouleurCitron\WPUtils;

use CouleurCitron\WPUtils\Exceptions\InsertException;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use WP_Query;
use WP_Term;

abstract class Taxonomy {

    /**
     * @var int
     */
    protected static $posts_per_page;

    /**
     * @var bool
     */
    protected static $translatable = false;

    /**
     * Set object type(s) for the taxonomy
     *
     * @return string|array
     */
    abstract protected static function objectType();

    /**
     * Taxonomy configuration
     *
     * @see https://developer.wordpress.org/reference/functions/register_taxonomy/
     *
     * @return array
     */
    abstract protected static function config(): array;

    public static function name(): string {
        return Str::snake( ( new ReflectionClass( static::class ) )->getShortName() );
    }

    /**
     * Register the taxonomy
     */
    public static function register(): void {
        if ( empty( static::objectType() ) ) {
            throw new InvalidArgumentException( 'Object type for a taxonomy cannot be empty.' );
        }

        add_action( 'init', function () {
            $class = ( new ReflectionClass( static::class ) )->getShortName();

            $objectType = collect( Arr::wrap( static::objectType() ) )
                ->map( function ( $type ) {
                    return class_exists( $type ) ? $type::name() : $type;
                } )
                ->toArray();

            register_taxonomy( static::name(), $objectType, array_merge(
                [ 'rewrite' => [ 'slug' => Str::plural( Str::kebab( $class ) ) ] ],
                static::config()
            ) );
        } );

        if ( static::$posts_per_page ) {
            add_action( 'pre_get_posts', function ( WP_Query $query ) {
                if ( ! $query->is_admin && $query->is_main_query() && $query->is_tax( static::name() ) ) {
                    $query->set( 'posts_per_page', static::$posts_per_page );
                }
            } );
        }

        if ( static::$translatable ) {
            add_filter( 'pll_get_taxonomies', function ( $post_types ) {
                return array_merge( $post_types, [ static::name() => static::name() ] );
            } );
        }
    }

    /**
     * @param array $args
     *
     * @return Collection
     */
    public static function all( $args = [] ): Collection {
        return collect( get_terms( array_merge( [
            'taxonomy'   => static::name(),
            'hide_empty' => false,
        ], $args ) ) );
    }

    /**
     * @param int $id
     *
     * @return WP_Term
     */
    public static function find( $id ): WP_Term {
        return get_term( $id, static::name() );
    }

    /**
     * @param int|int[] $post_id
     *
     * @return Collection
     */
    public static function fromPost( $post_id ): Collection {
        return collect( wp_get_object_terms( $post_id, static::name() ) );
    }

    /**
     * @param string $term
     * @param array  $args
     *
     * @return WP_Term
     * @throws InsertException
     */
    public static function insert( $term, array $args = [] ): WP_Term {
        $id = wp_insert_term( $term, static::name(), $args );

        if ( is_wp_error( $id ) ) {
            throw InsertException::fromWPError( $id );
        }

        return get_term( $id['term_id'], static::name() );
    }

}
