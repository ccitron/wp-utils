<?php

use Illuminate\Container\Container;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Session\{Flash\FlashBagInterface, Session};

if ( ! function_exists( 'asset' ) ) {
    /**
     * @param string $path
     *
     * @return string
     * @deprecated Use get_theme_file_uri() instead
     */
    function asset( $path ) {
        return get_template_directory_uri() . '/' . $path;
    }
}

if ( ! function_exists( 'template_path' ) ) {
    /**
     * @param string $path
     *
     * @return string
     * @deprecated Use get_theme_file_path() instead
     */
    function template_path( $path ) {
        return get_template_directory() . '/' . ltrim( $path, '/' );
    }
}

if ( ! function_exists( 'app' ) ) {
    /**
     * Get the available container instance.
     *
     * @param string $abstract
     * @param array  $parameters
     *
     * @return mixed|Container
     */
    function app( $abstract = null, array $parameters = [] ) {
        if ( $abstract === null ) {
            return Container::getInstance();
        }

        return Container::getInstance()->make( $abstract, $parameters );
    }
}

if ( ! function_exists( 'session' ) ) {
    /**
     * Get / set the specified session value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param array|string $key
     * @param mixed        $value
     *
     * @return mixed|Session
     */
    function session( $key = null, $value = null ) {
        if ( $key === null ) {
            return app( 'session' );
        }

        if ( $value === null ) {
            return app( 'session' )->get( $key );
        }

        return app( 'session' )->set( $key, $value );
    }
}

if ( ! function_exists( 'flash' ) ) {
    /**
     * @param string|array $type
     * @param mixed        $value
     *
     * @return array|FlashBagInterface
     */
    function flash( $type = null, $value = null ) {
        if ( $type === null ) {
            return session()->getFlashBag();
        }

        if ( is_array( $type ) && $value === null ) {
            foreach ( $type as $key => $v ) {
                session()->getFlashBag()->add( $key, $v );
            }

            return;
        }

        if ( $value === null ) {
            return Arr::first( session()->getFlashBag()->get( $type ) );
        }

        return session()->getFlashBag()->add( $type, $value );
    }
}

if ( ! function_exists( 'request' ) ) {
    /**
     * @param string $key
     * @param mixed  $default
     *
     * @return \Symfony\Component\HttpFoundation\Request|mixed
     */
    function request( $key = null, $default = null ) {
        if ( $key === null ) {
            return app( 'request' );
        }

        return app( 'request' )->get( $key, $default );
    }
}

if ( ! function_exists( 'breadcrumb' ) ) {
    function breadcrumb( $before = '', $after = '' ) {
        if ( function_exists( 'yoast_breadcrumb' ) ) {
            ?>
            <div class="breadcrumb">
                <?php yoast_breadcrumb( $before, $after ) ?>
            </div>
            <?php
        }
    }
}

if ( ! function_exists( 'svg' ) ) {
    function svg( $path ) {
        echo file_get_contents( get_theme_file_path( $path ) );
    }
}

if ( ! function_exists( 'array_insert' ) ) {
    /**
     * @param array $array
     * @param array $value
     * @param int   $index
     *
     * @return array
     */
    function array_insert( $array, $value, $index ) {
        return array_merge(
            array_slice( $array, 0, $index ),
            $value,
            array_slice( $array, $index )
        );
    }
}

if ( ! function_exists( 'array_filter_recursive' ) ) {
    /**
     * @param array    $input
     * @param callable $callback
     *
     * @return array
     */
    function array_filter_recursive( $input, $callback = null ) {
        $input = array_filter( $input, $callback );

        foreach ( $input as &$value ) {
            if ( is_array( $value ) ) {
                $value = array_filter_recursive( $value, $callback );
            }
        }

        return $input;
    }
}
