<?php

namespace CouleurCitron\WPUtils;

use CouleurCitron\WPUtils\Exceptions\InsertException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use ReflectionClass;
use WP_Post;
use WP_Query;

/**
 * Class PostType
 *
 * @package Valbiotis\Theme\PostTypes
 */
abstract class PostType {

    const SUPPORTS_TITLE = 'title';

    const SUPPORTS_EDITOR = 'editor';

    const SUPPORTS_COMMENTS = 'comments';

    const SUPPORTS_REVISIONS = 'revisions';

    const SUPPORTS_TRACKBACKS = 'trackbacks';

    const SUPPORTS_AUTHOR = 'author';

    const SUPPORTS_EXCERPT = 'excerpt';

    const SUPPORTS_PAGE_ATTRIBUTES = 'page-attributes';

    const SUPPORTS_THUMBNAIL = 'thumbnail';

    const SUPPORTS_CUSTOM_FIELDS = 'custom-fields';

    const SUPPORTS_POST_FORMATS = 'post-formats';

    const STATUS_PUBLISH = 'publish';

    const STATUS_FUTURE = 'future';

    const STATUS_DRAFT = 'draft';

    const STATUS_PENDING = 'pending';

    const STATUS_PRIVATE = 'private';

    const STATUS_TRASH = 'trash';

    /**
     * @var int
     */
    protected static $posts_per_page;

    /**
     * @var bool
     */
    protected static $translatable = false;

    private static function registerColumns(): void {
        foreach ( static::columns() as $column ) {
            $column->register( static::name() );
        }
    }

    /**
     * Post type configuration
     *
     * @see https://developer.wordpress.org/reference/functions/register_post_type/
     *
     * @return array
     */
    abstract protected static function config(): array;

    /**
     * @return Column[]
     */
    protected static function columns(): array {
        return [];
    }

    /**
     * @return string
     */
    public static function name() {
        return Str::limit( Str::snake( ( new ReflectionClass( static::class ) )->getShortName() ), 20, '' );
    }

    /**
     * Register the taxonomy
     */
    public static function register() {
        add_action( 'init', function () {
            $class = ( new ReflectionClass( static::class ) )->getShortName();

            register_post_type( static::name(), array_merge(
                [ 'rewrite' => [ 'slug' => Str::plural( Str::kebab( $class ) ) ] ],
                static::config()
            ) );
        } );

        if ( static::$posts_per_page && ( static::config()['has_archive'] ?? false ) ) {
            add_action( 'pre_get_posts', function ( WP_Query $query ) {
                if ( ! $query->is_admin && $query->is_main_query() && $query->is_post_type_archive( static::name() ) ) {
                    $query->set( 'posts_per_page', static::$posts_per_page );
                }
            } );
        }

        if ( static::$translatable ) {
            add_filter( 'pll_get_post_types', function ( $post_types ) {
                return array_merge( $post_types, [ static::name() => static::name() ] );
            } );
        }

        if ( count( static::columns() ) ) {
            static::registerColumns();
        }
    }

    /**
     * @param array $args
     *
     * @return Collection
     */
    public static function all( $args = [] ) {
        return collect( get_posts( array_merge( [
            'post_type'        => static::name(),
            'nopaging'         => true,
            'suppress_filters' => false,
        ], $args ) ) );
    }

    /**
     * @param array $args
     *
     * @return WP_Post
     * @throws InsertException
     */
    public static function insert( array $args ) {
        $id = wp_insert_post( array_merge( $args, [
            'post_type' => static::name(),
        ] ), true );

        if ( is_wp_error( $id ) ) {
            throw InsertException::fromWPError( $id );
        }

        return get_post( $id );
    }

}
