<?php

namespace CouleurCitron\WPUtils;

use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AjaxForm extends Form {

    public function callback(): void {
        try {
            $response = $this->handle( $this->request );
        } catch ( ValidationException $e ) {
            $response = new JsonResponse( [
                'errors' => $e->validator->errors(),
            ], 422 );
        }
        $response->send();
        die();
    }

    public static function register(): void {
        $instance = app( static::class );
        if ( $instance->admin ) {
            add_action( 'wp_ajax_' . $instance->action(), [ $instance, 'callback' ] );
        }
        if ( $instance->public ) {
            add_action( 'wp_ajax_nopriv_' . $instance->action(), [ $instance, 'callback' ] );
        }
    }

}
