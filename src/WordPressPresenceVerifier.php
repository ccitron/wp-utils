<?php

namespace CouleurCitron\WPUtils;

use Illuminate\Validation\PresenceVerifierInterface;

class WordPressPresenceVerifier implements PresenceVerifierInterface {

    const WP_COLUMNS = [
        'ID',
        'post_author',
        'post_name',
        'post_type',
        'post_title',
        'post_date',
        'post_date_gmt',
        'post_content',
        'post_excerpt',
        'post_status',
        'comment_status',
        'ping_status',
        'post_password',
        'post_parent',
        'post_modified',
        'post_modified_gmt',
        'comment_count',
        'menu_order',
    ];

    /**
     * Count the number of objects in a collection having the given value.
     *
     * @param string $collection
     * @param string $column
     * @param string $value
     * @param int    $excludeId
     * @param string $idColumn
     * @param array  $extra
     *
     * @return int
     */
    public function getCount( $collection, $column, $value, $excludeId = null, $idColumn = null, array $extra = [] ) {
        $params = $this->prepareQueryParams( $collection, $column, $value );

        return count( get_posts( $params ) );
    }

    /**
     * Count the number of objects in a collection with the given values.
     *
     * @param string $collection
     * @param string $column
     * @param array  $values
     * @param array  $extra
     *
     * @return int
     */
    public function getMultiCount( $collection, $column, array $values, array $extra = [] ) {
        $params = $this->prepareQueryParams( $collection, $column, $values );

        return count( get_posts( $params ) );
    }

    public function setConnection() {
        //
    }

    /**
     * @param string      $collection
     * @param string      $column
     * @param mixed|array $values
     *
     * @return array
     */
    protected function prepareQueryParams( $collection, $column, $values ) {
        $params = [
            'post_type'  => $collection,
            'nopaging'   => true,
            'meta_query' => [],
        ];

        if ( strtolower( $column ) === 'id' ) {
            $params[ is_array( $values ) ? 'post__in' : 'p' ] = $values;
        } else if ( ! in_array( $column, self::WP_COLUMNS, true ) ) {
            $params['meta_query'][] = [
                'key'     => $column,
                'value'   => $values,
                'compare' => is_array( $values ) ? 'IN' : '=',
            ];
        }

        return $params;
    }
}
