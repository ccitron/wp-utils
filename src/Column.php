<?php

namespace CouleurCitron\WPUtils;

use Illuminate\Support\Str;
use WP_Query;

class Column {

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $label;

    /**
     * @var int
     */
    public $order;

    /**
     * @var bool
     */
    public $sortable = false;

    /**
     * @var callable
     */
    public $sortCallback;

    /**
     * @var callable
     */
    public $renderCallback;

    /**
     * Column constructor.
     *
     * @param string $name
     * @param string $label
     */
    public function __construct( $name, $label = '' ) {
        $this->name  = $name;
        $this->label = $label ?: Str::title( str_replace( '_', ' ', $name ) );
    }

    /**
     * @param string $name
     * @param string $label
     *
     * @return Column
     */
    public static function make( $name, $label = '' ): Column {
        return new static( $name, $label );
    }

    /**
     * @param string $label
     *
     * @return $this
     */
    public function label( $label ): Column {
        $this->label = $label;

        return $this;
    }

    /**
     * @param int $order
     *
     * @return $this
     */
    public function order( $order ): Column {
        $this->order = $order;

        return $this;
    }

    /**
     * @param bool $sortable
     *
     * @return $this
     */
    public function sortable( $sortable = true ): Column {
        $this->sortable = $sortable;

        return $this;
    }

    /**
     * @param callable $callback
     *
     * @return $this
     */
    public function sort( $callback ): Column {
        $this->sortable     = true;
        $this->sortCallback = $callback;

        return $this;
    }

    /**
     * @param callable $callback
     *
     * @return $this
     */
    public function render( $callback ): Column {
        $this->renderCallback = $callback;

        return $this;
    }

    /**
     * @param string $post_type
     */
    public function register( $post_type ): void {
        add_filter( "manage_{$post_type}_posts_columns", function ( array $columns ) {
            return array_insert(
                $columns,
                [ $this->name => $this->label ],
                $this->order ?? count( $columns )
            );
        } );

        add_action( "manage_{$post_type}_posts_custom_column", function ( $column, $post_id ) {
            if ( $this->name === $column ) {
                ( $this->renderCallback )( $post_id );
            }
        }, 10, 2 );

        if ( $this->sortable ) {
            add_filter( "manage_edit-{$post_type}_sortable_columns", function ( $columns ) {
                return array_merge( $columns, [ $this->name => $this->name ] );
            } );

            add_action( 'pre_get_posts', function ( WP_Query $query ) use ( $post_type ) {
                if ( $query->is_admin && $query->is_main_query() && $query->get( 'post_type' ) === $post_type ) {
                    ( $this->sortCallback )( $query );
                }
            } );
        }
    }

}
