<?php

namespace CouleurCitron\WPUtils\Rules;

use Illuminate\Contracts\Validation\Rule;

class Nonce implements Rule {

    /**
     * @var string
     */
    protected $action;

    /**
     * Nonce constructor.
     *
     * @param string $action
     */
    public function __construct( $action ) {
        $this->action = $action;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes( $attribute, $value ) {
        return wp_verify_nonce( $value, $this->action );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return app( 'translator' )->get( 'validation.nonce' );
    }
}