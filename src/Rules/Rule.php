<?php

namespace CouleurCitron\WPUtils\Rules;

use Illuminate\Validation\Rule as IlluminateRule;

class Rule extends IlluminateRule {

    /**
     * @param string $attribute
     * @param string ...$values
     *
     * @return string
     */
    public static function requiredIf( $attribute, ...$values ) {
        return "required_if:{$attribute}," . implode( ',', $values );
    }

}