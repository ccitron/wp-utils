<?php

namespace CouleurCitron\WPUtils\Rules;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Contracts\Validation\Rule;

class Recaptcha implements Rule {

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes( $attribute, $value ) {
        $http = new Client();
        try {
            $response = $http->post( 'https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => [
                    'secret'   => env( 'GOOGLE_RECAPTCHA_SECRET_KEY' ),
                    'response' => $value,
                ],
            ] );
        } catch ( Exception $e ) {
            return false;
        }

        $response = json_decode( $response->getBody()->getContents(), JSON_OBJECT_AS_ARRAY );

        return $response && $response['success'];
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return app( 'translator' )->get( 'validation.recaptcha' );
    }
}