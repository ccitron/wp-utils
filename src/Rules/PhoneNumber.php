<?php

namespace CouleurCitron\WPUtils\Rules;

use Illuminate\Contracts\Validation\Rule;
use libphonenumber\{NumberParseException, PhoneNumberUtil};

class PhoneNumber implements Rule {

    /**
     * @var string
     */
    protected $region;

    /**
     * PhoneNumber constructor.
     *
     * @param string $region
     */
    public function __construct( $region ) {
        $this->region = $region;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes( $attribute, $value ) {
        try {
            $phoneNumber = PhoneNumberUtil::getInstance()->parse( $value, $this->region );
        } catch ( NumberParseException $e ) {
            return false;
        }

        return PhoneNumberUtil::getInstance()->isValidNumber( $phoneNumber );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return app( 'translator' )->get( 'validation.phone_number' );
    }
}