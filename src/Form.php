<?php

namespace CouleurCitron\WPUtils;

use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\{Factory, ValidationException, Validator};
use Symfony\Component\HttpFoundation\{File\UploadedFile, RedirectResponse, Request, Response};

abstract class Form {

    /** @var bool */
    protected $public = true;

    /** @var bool */
    protected $admin = true;

    /** @var Request */
    protected $request;

    /** @var Factory */
    protected $validation;

    /** @var Validator */
    protected $validator;

    /**
     * @param array $data
     * @param array $rules
     *
     * @return Collection
     * @throws ValidationException
     */
    protected function validate( array $data, array $rules ): Collection {
        $this->validator = $this->validation->make( $data, $rules );
        $this->validator->validate();

        return collect( $this->validator->getData() );
    }

    /**
     * @return string
     */
    protected function redirectTo(): string {
        return $this->request->get( '_wp_http_referer' );
    }

    /**
     * @return RedirectResponse
     */
    protected function back(): RedirectResponse {
        return new RedirectResponse( $this->redirectTo() );
    }

    /**
     * @return MessageBag
     */
    protected function errors(): MessageBag {
        return $this->validator->errors();
    }

    /**
     * Form constructor.
     *
     * @param Request $request
     * @param Factory $validation
     */
    public function __construct( Request $request, Factory $validation ) {
        $this->request    = $request;
        $this->validation = $validation;
    }

    /**
     * @return string
     */
    abstract public function action(): string;

    /**
     * @param Request $request
     *
     * @return Response
     * @throws ValidationException
     */
    abstract public function handle( Request $request ): Response;

    public function callback(): void {
        try {
            $response = $this->handle( $this->request );
        } catch ( ValidationException $e ) {
            flash( [
                'errors' => $e->validator->errors(),
                'old'    => collect(
                    array_filter_recursive( $e->validator->getData(), function ( $value ) {
                        return ! ( $value instanceof UploadedFile );
                    } )
                ),
            ] );
            $response = $this->back();
        }
        $response->send();
        die();
    }

    public static function register(): void {
        $instance = app( static::class );
        if ( $instance->admin ) {
            add_action( 'admin_post_' . $instance->action(), [ $instance, 'callback' ] );
        }
        if ( $instance->public ) {
            add_action( 'admin_post_nopriv_' . $instance->action(), [ $instance, 'callback' ] );
        }
    }

}
