<?php
/**
 * Plugin Name: WP Utils
 * Description: WordPress code utilities
 * Version: 1.0.0
 * Author: Couleur Citron
 * Author URI: https://www.couleur-citron.com/
 * License: MIT License
 */

use CouleurCitron\WPUtils\WordPressPresenceVerifier;
use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\{FileLoader, Translator};
use Illuminate\Validation\Factory;
use Symfony\Component\HttpFoundation\{Request, Session\Session};

$container = Container::getInstance();
$container->bind( 'locale', function () {
    return get_locale();
} );
$container->singleton( Request::class, function () {
    return Request::createFromGlobals();
} );
$container->alias( Request::class, 'request' );
$container->singleton( 'session', function () {
    return new Session();
} );
$container->singleton( 'filesystem', function () {
    return new Filesystem();
} );
$container->singleton( 'translation.loader', function ( $app ) {
    return new FileLoader( $app['filesystem'], get_theme_file_path( 'languages' ) );
} );
$container->singleton( 'translator', function ( $app ) {
    return new Translator( $app['translation.loader'], explode( '_', $app['locale'] )[0] );
} );
$container->singleton( Factory::class, function ( $app ) {
    $validation = new Factory( $app['translator'], $app );
    $validation->setPresenceVerifier( new WordPressPresenceVerifier() );

    return $validation;
} );
$container->singleton( wpdb::class, function () {
    global $wpdb;

    return $wpdb;
} );
$container->alias( Factory::class, 'validator' );

add_action( 'after_setup_theme', function () {
    try {
        session()->start();
    } catch ( RuntimeException $e ) {
    }
} );

add_action( 'wp_login', function () {
    session()->invalidate();
} );

add_action( 'wp_logout', function () {
    session()->invalidate();
} );
